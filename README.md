# KineticAsymmetryProfiles
An interactive tutorial on how to use kinetic asymmetry profiles

The supplementary code is made available as a .ipynb file and uses an embedded dataset that serves as a basis to run the code.
It can be executed using Jupyter Notebook environment in any operating system or directly online following this link: 

https://mybinder.org/v2/gl/emanuele.penocchio%2Fengspan/93749617cc35a21a4f7471233888890fd5fef6ce?urlpath=/tree/%2Ftutorial.ipynb

1. Systems requirements

Software dependencies are indicated:
- Python 3.6+
- numpy 1.16+
- matplotlib 3.3+
- scipy 1.4+
- IPython 7+
- Jupyter Notebook

The software has been tested on Linux (Ubuntu 20.04.3 LTS), Windows (10-pro) and macOS (High Sierra 10.13.6).

No non-standard hardware is required.

2. Installation guide
Download the file tutorial.ipynb and scheme.png from this repository.
The code is ready to run in the Jupyter Notebook environment provided that the above requirements are satisfied.

3. Demo
To run the code, open tutorial.ipynb with Jupyter Notebook and follow the indication written in the first block.

The expected output is an interactive figure.

The expected runtime on a "normal computer" is a few seconds.

4. Instructions for use
To run the software on your data, use the interactive sliders to set parameters to the values of choice.

Kinetic asymmetry profiles with a different number of species can be drawn with the "kozn" function, which can be copy-pasted and used as a standalone program to draw (non interactive) arbitrarly large kinetic surfaces.
To reproduce all the results of the manuscript, the "kozn" function can be used with the data in the SI to draw all the kinetic sufaces in the main text.
